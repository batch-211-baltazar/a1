My motivation in joining the bootcamp is to get the self satisfaction that I am improving in my knowledge and in my skills.
Thus, it will help me succeed through my career path.

Plus, I do this all to make my self better in terms of work and earning for my family.


As for my work experience,
I have been in the field of customer service for 3 years now. From being a customer service agent to technical support, then I was given an opportunity to be a Workshop Host, and now a Trainer.

I have handled accounts and troubleshooting concerns in various items and products wherein I have showcased all my skills in the means of communication, technical, problem solving and more.

I was awarded as Top 6 on all sites for Quality Assurance and a Top Rated Freelancer in Upwork.

And I am looking forward to enter the field of Web Dev.